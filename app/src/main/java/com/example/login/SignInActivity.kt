package com.example.login

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_sign_in.*

class SignInActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)
        init()
    }

    private fun init() {
        auth = Firebase.auth
        signInButton.setOnClickListener {
            signIn()
        }
    }

    private fun signIn() {
        val email = emailSignInEditText.text.toString()
        val password = passwordSignInEditText.text.toString()
        if (email.isNotEmpty() && password.isNotEmpty()) {
            progressBarSignIn.visibility = View.VISIBLE
            signInButton.isClickable = false
            auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this) { task ->
                    progressBarSignIn.visibility = View.GONE
                    signInButton.isClickable = true
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        d("signIn", "signInWithEmail:success")
                        val user = auth.currentUser
                        Toast.makeText(this, "Authentication is Success!", Toast.LENGTH_SHORT).show()

                    } else {
                        // If sign in fails, display a message to the user.
                        d("signIn", "signInWithEmail:failure", task.exception)
                        val report = task.exception
                        Toast.makeText(baseContext, "Authentication failed.$report",
                            Toast.LENGTH_SHORT).show()

                        // ...
                    }

                    // ...
                }
        } else {
            Toast.makeText(this, "Please fill all fields", Toast.LENGTH_SHORT).show()
        }
    }
}